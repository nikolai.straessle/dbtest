package datageneration

import (
	"dbtest/entities"
	"fmt"
	"testing"
)

func TestDatagenerator_Generate(t *testing.T) {
	type args struct {
		numberOfDataSets   int
		numberOfChannels   int
		processingFunction func(dataSet entities.DataSet) error
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Success",
			args: args{
				numberOfDataSets: 1000,
				numberOfChannels: 10,
				processingFunction: func(dataSet entities.DataSet) error {
					fmt.Println(dataSet.Id)
					return nil
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Datagenerator{}
			d.Generate(tt.args.numberOfDataSets, tt.args.numberOfChannels, tt.args.processingFunction)
		})
	}
}
