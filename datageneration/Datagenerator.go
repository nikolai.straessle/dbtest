package datageneration

import (
	"dbtest/entities"
	"fmt"
	"github.com/ahmetask/worker"
	"github.com/google/uuid"
	"math/rand"
	"time"
)

type Datagenerator struct{}

type Job struct {
	Id                 int
	processingFunction func(dataSet entities.DataSet) error
}

func (j *Job) Do() {
	data := entities.DataSet{
		Id:   uuid.New().String(),
		Data: []entities.Entry{},
	}
	inputDate := time.Now()
	for i := 0; i < 96; i++ {
		if i%1000000 == 0 {
			inputDate = inputDate.AddDate(0, 0, 1)
		}
		entry := entities.Entry{
			TimeSlot:          uint(i),
			ConsumerProducers: []entities.ConsumerProducers{},
		}
		data.Date = inputDate
		for k := 0; k < 10; k++ {
			entry.ConsumerProducers = append(entry.ConsumerProducers, entities.ConsumerProducers{
				Name:  fmt.Sprintf("consumer-%d", k),
				Value: rand.Float64(),
			})
		}
		data.Data = append(data.Data, entry)
	}
	if err := j.processingFunction(data); nil != err {

	}
}

func (d *Datagenerator) Generate(numberOfDataSets, numberOfChannels int, processingFunction func(dataSet entities.DataSet) error) {
	pool := worker.NewWorkerPool(numberOfChannels, numberOfDataSets)
	pool.Start()
	for i := 0; i < numberOfDataSets; i++ {
		pool.Submit(&Job{Id: i, processingFunction: processingFunction})
	}
	//Stop Worker Pool
	pool.Stop()
}
