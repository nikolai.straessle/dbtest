package main

import (
	"dbtest/datageneration"
	"dbtest/datastorage"
	"dbtest/datastorage/cassandra"
	"dbtest/datastorage/elastic"
	hBase "dbtest/datastorage/hbase"
	"dbtest/datastorage/mongo"
	"dbtest/datastorage/mysql"
	"dbtest/datastorage/postgres"
	"dbtest/datastorage/redis"
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"log"
	"os"
)

func SetLogger(db string) {
	f, err := os.OpenFile(fmt.Sprintf("%s.log", db), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	// Log as JSON instead of the default ASCII formatter.
	logrus.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(f)

	// Only log the warning severity or above.
	logrus.SetLevel(logrus.InfoLevel)
}

func main() {
	db := flag.String("db", "mysql", "the used database system")
	flag.Parse()

	SetLogger(*db)

	var m datastorage.DataStorage
	switch *db {
	case "mysql":
		m = mysql.NewMySQL("root:dbtest@/dbtest")
	case "mongo":
		m = mongo.NewMongo("mongodb://root:example@localhost:27017/")
	case "postgres":
		m = postgres.NewPostgres("host=localhost port=5432 user=postgres password=dbtest dbname=dbtest sslmode=disable")
	case "elastic":
		m = elastic.NewElastic([]string{"https://localhost:9200"}, "dmh4dC1IOEIyUFFLZ0hYR2VLVC06dGNNZEVqclBRdEdaN013dzF4ckZLdw==")
	case "redis":
		m = redis.NewRedis("localhost:6379")
	case "cassandra":
		m = cassandra.NewCassandra("localhost:9042")
	case "hbase":
		m = hBase.NewHBase("localhost")
	}

	generator := datageneration.Datagenerator{}
	generator.Generate(10000000, 1000, m.StoreData)
}
