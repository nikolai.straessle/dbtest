SELECT MAX(t.value)
FROM dbtest d,
     JSON_TABLE(d.data, '$' COLUMNS (
         id VARCHAR(100) PATH '$.id',
         date DATETIME PATH '$.date',
         NESTED PATH '$.data[*]' COLUMNS (
             time_slot INT PATH '$.time_slot',
             NESTED PATH '$.consumer_producers[*]' COLUMNS (
                 name VARCHAR(100) PATH '$.name',
                 value FLOAT PATH '$.value'
                 )
             )
         )
         ) AS t
WHERE t.name = 'consumer-0';