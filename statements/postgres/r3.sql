SELECT max(d.c1)
FROM (
         SELECT (cp -> 'value')::float AS c1
         FROM dbtest,
              jsonb_array_elements(data -> 'data') entries,
              jsonb_array_elements(entries -> 'consumer_producers') cp
         WHERE cp ->> 'name' = 'consumer-0'
           AND (data ->> 'date')::timestamp
         with time zone < '2022-03-29 15:30:00':: timestamp
         with time zone
             LIMIT 1000000
     ) as d;