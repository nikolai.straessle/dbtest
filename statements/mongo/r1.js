db.dbtest.aggregate(
    [
        {$unwind: "$data"},
        {$unwind: "$data.consumer_producers"},
        {$group: {_id: "$_id", avgAge: {$avg: "$data.consumer_producers.value"}}}
    ]
    )