var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("dbtest");
    dbo.aggregate(
        [
            {$unwind: "$data"},
            {$unwind: "$data.consumer_producers"},
            {$group: {_id: "$_id", avgAge: {$max: "$data.consumer_producers.value"}}}
        ]
    )
});