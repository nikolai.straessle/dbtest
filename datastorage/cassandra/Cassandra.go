package cassandra

import (
	"dbtest/entities"
	"github.com/gocql/gocql"
	"github.com/sirupsen/logrus"
	"time"
)

type Cassandra struct {
	cluster *gocql.ClusterConfig
}

func NewCassandra(uri string) *Cassandra {
	cluster := gocql.NewCluster(uri)
	cluster.ProtoVersion = 4
	cluster.Consistency = gocql.One
	cluster.Timeout = 1 * time.Minute
	cluster.NumConns = 1
	cluster.Compressor = gocql.SnappyCompressor{}
	session, err := cluster.CreateSession()
	if err != nil {
		panic(err)
	}
	defer session.Close()
	err = session.Query("CREATE KEYSPACE IF NOT EXISTS dbtest WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };").Exec()
	if err != nil {
		panic(err)
	}
	err = session.Query("CREATE TABLE IF NOT EXISTS dbtest.dbtest( id UUID, date timestamp, timeslot int, name text, value double, PRIMARY KEY (id, timeslot, name));").Exec()
	if err != nil {
		panic(err)
	}
	cluster.Keyspace = "dbtest"
	return &Cassandra{
		cluster: cluster,
	}
}

func (m *Cassandra) StoreData(dataSet entities.DataSet) error {
	session, err := m.cluster.CreateSession()
	if err != nil {
		return err
	}
	defer session.Close()
	b := session.NewBatch(gocql.UnloggedBatch)
	start := time.Now()
	for _, e := range dataSet.Data {
		for _, cp := range e.ConsumerProducers {
			b.Query(`INSERT INTO dbtest.dbtest (id, date, timeslot, name, value) VALUES (?, ?, ?, ?, ?)`,
				dataSet.Id,
				dataSet.Date,
				e.TimeSlot,
				cp.Name,
				cp.Value,
			)
		}
	}
	if err := session.ExecuteBatch(b); nil != err {
		return err
	}
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "cassandra",
	}).Info("document stored")
	return nil
}
