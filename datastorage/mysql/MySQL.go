package mysql

import (
	"context"
	"database/sql"
	"dbtest/entities"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
	"time"
)

type MySQL struct {
	db *sql.DB
}

func NewMySQL(uri string) *MySQL {
	db, err := sql.Open("mysql", uri)
	if err != nil {
		panic(err)
	}
	// See "Important settings" section.
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(1000)
	_, err = db.ExecContext(context.Background(), "CREATE TABLE IF NOT EXISTS dbtest (id int auto_increment primary key, data JSON)")
	if err != nil {
		panic(err)
	}
	return &MySQL{
		db: db,
	}
}

func (m *MySQL) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	meta, err := m.db.Prepare("INSERT INTO dbtest (data) VALUES ( ? )")
	if err != nil {
		return err
	}
	data, err := json.Marshal(dataSet)
	if nil != err {
		return err
	}
	_, err = meta.ExecContext(context.Background(), data)
	if err != nil {
		return err
	}
	defer meta.Close()
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "mysql",
	}).Info("document stored")
	return nil
}
