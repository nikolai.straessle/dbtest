package neo4j

import (
	"dbtest/entities"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	"github.com/sirupsen/logrus"
	"time"
)

type Neo4j struct {
	client neo4j.Driver
}

func NewNeo4j(uri, username, password string) *Neo4j {
	driver, err := neo4j.NewDriver(uri, neo4j.BasicAuth(username, password, ""))
	if err != nil {
		panic(err)
	}
	return &Neo4j{
		client: driver,
	}
}

func (m *Neo4j) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	//encoded, err := json.Marshal(dataSet)
	//if err != nil {
	//	return err
	//}
	session := m.client.NewSession(neo4j.SessionConfig{})
	defer session.Close()
	//result, err := session.WriteTransaction(createItemFn)
	//if err != nil {
	//	return nil, err
	//}
	//err = m.client.Set(context.Background(), dataSet.Id, encoded, 0).Err()
	//if err != nil {
	//	return err
	//}
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "neo4j",
	}).Info("document stored")
	return nil
}
