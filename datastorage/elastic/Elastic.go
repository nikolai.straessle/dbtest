package elastic

import (
	"context"
	"crypto/tls"
	"dbtest/entities"
	"encoding/json"
	elasticsearch8 "github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/sirupsen/logrus"
	"net"
	"net/http"
	"strings"
	"time"
)

type Elastic struct {
	client *elasticsearch8.Client
}

func NewElastic(uri []string, apiKey string) *Elastic {
	es8, err := elasticsearch8.NewClient(elasticsearch8.Config{
		Addresses: uri,
		APIKey:    apiKey,
		Transport: &http.Transport{
			MaxIdleConns:          10000,
			MaxConnsPerHost:       10000,
			MaxIdleConnsPerHost:   10000,
			ResponseHeaderTimeout: time.Second,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	})
	if err != nil {
		panic(err)
	}
	req := esapi.IndicesCreateRequest{
		Index: "dbtest",
		Body: strings.NewReader(`{
  "settings": {
    "translog.durability": "async",
    "refresh_interval": "30s"
  }
}`),
	}
	_, err = req.Do(context.Background(), es8)
	if err != nil {
		panic(err)
	}
	return &Elastic{
		client: es8,
	}
}

func (m *Elastic) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	encoded, err := json.Marshal(dataSet)
	if err != nil {
		return err
	}
	req := esapi.IndexRequest{
		Index:      "dbtest",
		DocumentID: dataSet.Id,
		Body:       strings.NewReader(string(encoded)),
	}
	res, err := req.Do(context.Background(), m.client)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "elastic",
	}).Info("document stored")
	return nil
}
