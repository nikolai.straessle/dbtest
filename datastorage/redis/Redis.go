package redis

import (
	"context"
	"dbtest/entities"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"time"
)

type Redis struct {
	client *redis.Client
}

func NewRedis(uri string) *Redis {
	rdb := redis.NewClient(&redis.Options{
		Addr: uri,
	})
	return &Redis{
		client: rdb,
	}
}

func (m *Redis) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	encoded, err := json.Marshal(dataSet)
	if err != nil {
		return err
	}
	err = m.client.Do(context.Background(), "JSON.SET", fmt.Sprintf("dbtest:%s", dataSet.Id), "$", encoded).Err()
	if err != nil {
		return err
	}
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "redis",
	}).Info("document stored")
	return nil
}
