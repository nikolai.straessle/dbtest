package mongo

import (
	"context"
	"dbtest/entities"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Mongo struct {
	coll *mongo.Collection
}

func NewMongo(uri string) *Mongo {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	return &Mongo{
		coll: client.Database("dbtest").Collection("dbtest"),
	}
}

func (m *Mongo) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	_, err := m.coll.InsertOne(context.TODO(), dataSet)
	if err != nil {
		return err
	}
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "mongodb",
	}).Info("document stored")
	return nil
}
