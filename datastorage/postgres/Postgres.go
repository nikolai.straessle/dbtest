package postgres

import (
	"context"
	"database/sql"
	"dbtest/entities"
	"encoding/json"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"time"
)

type Postgres struct {
	db *sql.DB
}

func NewPostgres(uri string) *Postgres {
	db, err := sql.Open("postgres", uri)
	if err != nil {
		panic(err)
	}
	// See "Important settings" section.
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(1000)
	_, err = db.ExecContext(context.Background(), "CREATE TABLE IF NOT EXISTS dbtest (id SERIAL PRIMARY KEY, data JSONB)")
	if err != nil {
		panic(err)
	}
	return &Postgres{
		db: db,
	}
}

func (m *Postgres) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	meta, err := m.db.Prepare("INSERT INTO dbtest (data) VALUES ( $1 )")
	if err != nil {
		return err
	}
	data, err := json.Marshal(dataSet)
	if nil != err {
		return err
	}
	_, err = meta.ExecContext(context.Background(), data)
	if err != nil {
		return err
	}
	defer meta.Close()
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "postgres",
	}).Info("document stored")
	return nil
}
