package datastorage

import "dbtest/entities"

type DataStorage interface {
	StoreData(dataSet entities.DataSet) error
}
