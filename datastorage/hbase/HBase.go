package hBase

import (
	"context"
	"dbtest/entities"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/tsuna/gohbase"
	"github.com/tsuna/gohbase/hrpc"
	"time"
)

type HBase struct {
	session gohbase.Client
}

func NewHBase(uri string) *HBase {
	client := gohbase.NewClient(uri)
	return &HBase{
		session: client,
	}
}

func (m *HBase) StoreData(dataSet entities.DataSet) error {
	start := time.Now()
	data, err := json.Marshal(dataSet.Data)
	if nil != err {
		return err
	}
	values := map[string]map[string][]byte{"cf": {dataSet.Id: data}}
	putRequest, err := hrpc.NewPutStr(context.Background(), "dbtest", dataSet.Id, values)
	_, err = m.session.Put(putRequest)
	if err != nil {
		return err
	}
	elapsed := time.Since(start)
	logrus.WithFields(logrus.Fields{
		"Id":      dataSet.Id,
		"elapsed": elapsed.Milliseconds(),
		"source":  "hbase",
	}).Info("document stored")
	return nil
}
