package entities

import "time"

type DataSet struct {
	Id   string    `json:"id" cql:"id"`     // unique UUID
	Date time.Time `json:"date" cql:"date"` // date of the current plan
	Data []Entry   `json:"data" cql:"data"` // entries of the current day
}

type Entry struct {
	TimeSlot          uint                `json:"time_slot" cql:"timeslot"`                    // number between 0 and 95
	ConsumerProducers []ConsumerProducers `json:"consumer_producers" cql:"consumer_producers"` // entries of all consumers/producers
}

type ConsumerProducers struct {
	Name  string  `json:"name" cql:"name"`   // name of the consumer/producer
	Value float64 `json:"value" cql:"value"` //value of the current slot
}
